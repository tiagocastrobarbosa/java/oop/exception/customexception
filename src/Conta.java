public abstract class Conta {
	
	protected double saldo;
	
	public void deposita(double valor) {
		this.saldo += valor;
	}
	
	public void saca(double valor) {		
		
		if (this.saldo < valor) {
			throw new SaldoInsuficienteException("Saldo: " + this.saldo + ", Valor: " + valor);
		}		
		this.saldo -= valor;
		
	}
	
	public double getSaldo() {
		return this.saldo;
	}
	
}
