public class Programa {

	public static void main(String[] args) {
			
		Conta cc = new ContaCorrente();
		cc.deposita(500.0);
		cc.saca(700.0); // escreva aqui o valor a ser retirado do saldo e veja o resultado no Console
		System.out.println(cc.getSaldo());
		
		/*
		 * O programa certamente exige um tratamento de codigo com try-catch.
		 * Porem, meu programa nao esta tratando a excecao devidamente porque
		 * ele serve apenas como exemplificacao de um Exception personalizado.
		 */
	}
	
}
