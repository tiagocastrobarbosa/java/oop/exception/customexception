public class ContaCorrente extends Conta {
	
	@Override
	public void saca(double valor) {
		double valorComTaxa = valor + 3.0; // taxa fantasiosa de 3,00 reais
		super.saca(valorComTaxa);
	}
	
}
